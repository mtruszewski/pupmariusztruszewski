# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0037_auto_20160826_0131'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='wygrana',
            field=models.ForeignKey(to='blog.Team'),
        ),
    ]
