# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0034_auto_20160823_1857'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='match',
            name='bramki',
        ),
    ]
