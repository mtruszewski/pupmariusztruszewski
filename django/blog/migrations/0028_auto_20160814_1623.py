# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0027_auto_20160814_1621'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='goal1',
            field=models.IntegerField(default='-', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='match',
            name='goal2',
            field=models.IntegerField(default='-', null=True, blank=True),
        ),
    ]
