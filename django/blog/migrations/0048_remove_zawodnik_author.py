# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0047_auto_20161001_1138'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='zawodnik',
            name='author',
        ),
    ]
