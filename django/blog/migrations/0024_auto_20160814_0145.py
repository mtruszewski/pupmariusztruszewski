# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0023_auto_20160814_0137'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ad',
            name='button',
            field=models.ImageField(upload_to='buttons/', blank=True, null=True, default='buttons/noimage.png', width_field=100, height_field=50),
        ),
        migrations.AlterField(
            model_name='ad',
            name='link',
            field=models.URLField(blank=True, null=True),
        ),
    ]
