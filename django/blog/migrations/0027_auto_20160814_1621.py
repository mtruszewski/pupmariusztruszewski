# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0026_auto_20160814_0153'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='goal1',
            field=models.IntegerField(default='-'),
        ),
        migrations.AlterField(
            model_name='match',
            name='goal2',
            field=models.IntegerField(default='-'),
        ),
    ]
