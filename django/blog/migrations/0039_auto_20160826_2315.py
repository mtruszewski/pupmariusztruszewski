# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0038_auto_20160826_2314'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='wygrana',
            field=models.IntegerField(),
        ),
    ]
