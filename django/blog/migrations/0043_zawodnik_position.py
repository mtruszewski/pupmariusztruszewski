# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0042_auto_20160827_1721'),
    ]

    operations = [
        migrations.AddField(
            model_name='zawodnik',
            name='position',
            field=models.CharField(choices=[('FW', 'Forward'), ('MF', 'Midfielder'), ('DF', 'Defender'), ('GK', 'Goalkeeper')], max_length=2, default=1),
            preserve_default=False,
        ),
    ]
