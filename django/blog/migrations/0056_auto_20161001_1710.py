# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0055_auto_20161001_1656'),
    ]

    operations = [
        migrations.AddField(
            model_name='sklad',
            name='goalPlayer1',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='sklad',
            name='goalPlayer2',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='zawodnik',
            name='goals',
            field=models.IntegerField(default=0),
        ),
    ]
