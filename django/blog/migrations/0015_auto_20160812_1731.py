# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0014_auto_20160811_1925'),
    ]

    operations = [
        migrations.RenameField(
            model_name='match',
            old_name='team',
            new_name='team2',
        ),
        migrations.AddField(
            model_name='match',
            name='team1',
            field=models.ForeignKey(to='blog.Team', related_name='team1', null=True),
        ),
    ]
