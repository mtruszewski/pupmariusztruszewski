# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0045_auto_20160827_2149'),
    ]

    operations = [
        migrations.AlterField(
            model_name='zawodnik',
            name='position',
            field=models.CharField(max_length=2, choices=[('GK', 'Goalkeeper'), ('DF', 'Defender'), ('MF', 'Midfielder'), ('FW', 'Forward')]),
        ),
        migrations.AlterField(
            model_name='zawodnik',
            name='side',
            field=models.CharField(max_length=3, choices=[('GK', 'GK'), ('L', 'Left'), ('C', 'Center'), ('R', 'Right'), ('L/R', 'Left/Right'), ('L/C', 'Left/Center'), ('C/R', 'Center/Right'), ('L/C/R', 'Left/Center/Right')]),
        ),
    ]
