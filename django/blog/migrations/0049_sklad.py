# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0048_remove_zawodnik_author'),
    ]

    operations = [
        migrations.CreateModel(
            name='Sklad',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('listaZawodnikow', models.CharField(max_length=400, default=0, help_text='Save to see')),
                ('match', models.ForeignKey(to='blog.Match')),
                ('team', models.ForeignKey(to='blog.Team')),
                ('team1Players', models.ForeignKey(null=True, related_name='team1Players', to='blog.Zawodnik')),
                ('team2Players', models.ForeignKey(null=True, related_name='team2Players', to='blog.Zawodnik')),
            ],
        ),
    ]
