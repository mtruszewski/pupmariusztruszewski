# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0059_auto_20161002_1214'),
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('book_name', models.CharField(unique=True, max_length=32)),
                ('author_name', models.CharField(unique=True, max_length=32)),
                ('publisher_name', models.CharField(unique=True, max_length=32)),
            ],
        ),
        migrations.CreateModel(
            name='Tactics',
            fields=[
                ('id', models.AutoField(verbose_name='ID', auto_created=True, serialize=False, primary_key=True)),
                ('tactic', models.TextField()),
            ],
        ),
    ]
