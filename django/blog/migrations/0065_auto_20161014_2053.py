# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0064_auto_20161014_2040'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tactics',
            old_name='tacticData',
            new_name='tactic',
        ),
    ]
