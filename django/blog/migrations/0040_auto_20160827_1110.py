# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0039_auto_20160826_2315'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='match',
            name='porazka',
        ),
        migrations.RemoveField(
            model_name='match',
            name='remis',
        ),
        migrations.RemoveField(
            model_name='match',
            name='wygrana',
        ),
        migrations.AddField(
            model_name='team',
            name='draw_away',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='team',
            name='draw_home',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='team',
            name='lose_away',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='team',
            name='lose_home',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='team',
            name='win_away',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='team',
            name='win_home',
            field=models.IntegerField(default=0),
        ),
    ]
