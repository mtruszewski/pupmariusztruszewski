# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0011_auto_20160811_1620'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='team',
            name='published_date',
        ),
    ]
