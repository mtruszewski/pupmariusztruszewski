# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0050_auto_20161001_1534'),
    ]

    operations = [
        migrations.AddField(
            model_name='zawodnik',
            name='nationality',
            field=models.CharField(default='PL', max_length=2),
        ),
        migrations.AddField(
            model_name='zawodnik',
            name='number',
            field=models.IntegerField(default=0),
        ),
    ]
