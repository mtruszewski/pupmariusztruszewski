# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0028_auto_20160814_1623'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='goal1',
            field=models.IntegerField(null=True, blank=True, default='-', verbose_name='-'),
        ),
        migrations.AlterField(
            model_name='match',
            name='goal2',
            field=models.IntegerField(null=True, blank=True, default='-', verbose_name='-'),
        ),
    ]
