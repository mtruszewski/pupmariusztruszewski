# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0049_sklad'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='sklad',
            name='listaZawodnikow',
        ),
        migrations.RemoveField(
            model_name='sklad',
            name='team',
        ),
    ]
