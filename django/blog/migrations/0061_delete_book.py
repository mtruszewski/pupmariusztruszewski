# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0060_book_tactics'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Book',
        ),
    ]
