# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0041_auto_20160827_1618'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='stracone_away',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='team',
            name='stracone_home',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='team',
            name='strzelone_away',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='team',
            name='strzelone_home',
            field=models.IntegerField(default=0),
        ),
    ]
