# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0017_auto_20160812_1836'),
    ]

    operations = [
        migrations.AddField(
            model_name='match',
            name='created_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
        migrations.AddField(
            model_name='match',
            name='published_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
