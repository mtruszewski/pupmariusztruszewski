# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0054_zawodnik_dateofbirth'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='zawodnik',
            name='age',
        ),
        migrations.AlterField(
            model_name='zawodnik',
            name='dateOfBirth',
            field=models.DateField(default=django.utils.timezone.now),
        ),
    ]
