# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0036_match_wygrana'),
    ]

    operations = [
        migrations.AddField(
            model_name='match',
            name='porazka',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='match',
            name='remis',
            field=models.IntegerField(default=0),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='match',
            name='wygrana',
            field=models.IntegerField(),
        ),
    ]
