# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0035_remove_match_bramki'),
    ]

    operations = [
        migrations.AddField(
            model_name='match',
            name='wygrana',
            field=models.CharField(max_length=400, default=0, help_text='Save to see'),
        ),
    ]
