# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0066_tactics_author'),
    ]

    operations = [
        migrations.AddField(
            model_name='tactics',
            name='created_date',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
