# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0024_auto_20160814_0145'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ad',
            name='button',
            field=models.ImageField(blank=True, null=True, width_field=100, height_field=50, upload_to='buttons/'),
        ),
        migrations.AlterField(
            model_name='ad',
            name='link',
            field=models.URLField(),
        ),
    ]
