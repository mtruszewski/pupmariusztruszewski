# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0056_auto_20161001_1710'),
    ]

    operations = [
        migrations.AddField(
            model_name='zawodnik',
            name='photo',
            field=models.ImageField(upload_to='photo/', null=True, blank=True),
        ),
    ]
