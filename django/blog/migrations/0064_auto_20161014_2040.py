# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0063_auto_20161014_2014'),
    ]

    operations = [
        migrations.RenameField(
            model_name='tactics',
            old_name='tactic',
            new_name='tacticData',
        ),
    ]
