# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0053_auto_20161001_1616'),
    ]

    operations = [
        migrations.AddField(
            model_name='zawodnik',
            name='dateOfBirth',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
