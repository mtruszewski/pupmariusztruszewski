# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0043_zawodnik_position'),
    ]

    operations = [
        migrations.AlterField(
            model_name='zawodnik',
            name='position',
            field=models.CharField(max_length=10, choices=[('Forward', (('L', 'Left'), ('C', 'Center'), ('R', 'Right'))), ('Midfielder', (('L', 'Left'), ('C', 'Center'), ('R', 'Right'))), ('Defender', (('L', 'Left'), ('C', 'Center'), ('R', 'Right'))), ('GK', 'Goalkeeper')]),
        ),
    ]
