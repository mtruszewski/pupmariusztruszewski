# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0030_auto_20160815_0037'),
    ]

    operations = [
        migrations.AddField(
            model_name='post',
            name='obrazek',
            field=models.ImageField(upload_to='post/', blank=True, null=True),
        ),
    ]
