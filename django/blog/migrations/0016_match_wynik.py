# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0015_auto_20160812_1731'),
    ]

    operations = [
        migrations.AddField(
            model_name='match',
            name='wynik',
            field=models.IntegerField(default=0, help_text='Save to see'),
        ),
    ]
