# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0058_auto_20161001_1739'),
    ]

    operations = [
        migrations.AddField(
            model_name='sklad',
            name='timePlayedP1',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='sklad',
            name='timePlayedP2',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='zawodnik',
            name='timePlayed',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='sklad',
            name='team1Players',
            field=models.ForeignKey(null=True, to='blog.Zawodnik', blank=True, related_name='team1Players'),
        ),
        migrations.AlterField(
            model_name='sklad',
            name='team2Players',
            field=models.ForeignKey(null=True, to='blog.Zawodnik', blank=True, related_name='team2Players'),
        ),
        migrations.AlterField(
            model_name='zawodnik',
            name='nationality',
            field=models.CharField(choices=[('PL', 'Poland'), ('BG', 'Bulgaria'), ('HR', 'Croatia'), ('CZ', 'Czech Republic'), ('DE', 'Germany'), ('HU', 'Hungary'), ('PT', 'Portugal'), ('RS', 'Serbia'), ('SI', 'Slovenia'), ('SK', 'Slovakia')], max_length=2),
        ),
    ]
