# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0022_remove_ad_link'),
    ]

    operations = [
        migrations.AddField(
            model_name='ad',
            name='button',
            field=models.ImageField(height_field=50, default='buttons/noimage.png', upload_to='buttons/%d/%m/%y/', width_field=100),
        ),
        migrations.AddField(
            model_name='ad',
            name='link',
            field=models.URLField(default='none'),
            preserve_default=False,
        ),
    ]
