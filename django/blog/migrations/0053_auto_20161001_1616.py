# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0052_auto_20161001_1552'),
    ]

    operations = [
        migrations.AlterField(
            model_name='zawodnik',
            name='nationality',
            field=models.CharField(choices=[('PL', 'Poland'), ('HR', 'Croatia'), ('DE', 'Germany')], max_length=2),
        ),
    ]
