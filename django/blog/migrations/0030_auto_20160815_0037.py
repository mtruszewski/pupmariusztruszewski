# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0029_auto_20160814_1627'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='goal1',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='match',
            name='goal2',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
