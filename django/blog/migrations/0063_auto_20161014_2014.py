# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0062_auto_20161014_1926'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tactics',
            name='tactic',
            field=models.CharField(max_length=10000),
        ),
    ]
