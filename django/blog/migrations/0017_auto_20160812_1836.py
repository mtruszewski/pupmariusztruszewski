# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0016_match_wynik'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='wynik',
            field=models.CharField(default=0, help_text='Save to see', max_length=400),
        ),
    ]
