# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0032_auto_20160816_1908'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='thumbnail',
            name='video',
        ),
        migrations.RemoveField(
            model_name='video',
            name='keywords',
        ),
        migrations.RemoveField(
            model_name='video',
            name='swf_url',
        ),
        migrations.AddField(
            model_name='video',
            name='published_date',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.DeleteModel(
            name='Thumbnail',
        ),
    ]
