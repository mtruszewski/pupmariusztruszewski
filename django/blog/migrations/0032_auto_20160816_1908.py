# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('blog', '0031_post_obrazek'),
    ]

    operations = [
        migrations.CreateModel(
            name='Thumbnail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('url', models.URLField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('video_id', models.CharField(unique=True, null=True, max_length=255, help_text='The Youtube id of the video')),
                ('title', models.CharField(null=True, blank=True, max_length=200)),
                ('description', models.TextField(null=True, blank=True)),
                ('keywords', models.CharField(help_text='Comma seperated keywords', null=True, blank=True, max_length=200)),
                ('youtube_url', models.URLField(null=True, blank=True, max_length=255)),
                ('swf_url', models.URLField(null=True, blank=True, max_length=255)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='thumbnail',
            name='video',
            field=models.ForeignKey(null=True, to='blog.Video'),
        ),
    ]
