# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0033_auto_20160816_1928'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='video',
            name='description',
        ),
        migrations.RemoveField(
            model_name='video',
            name='youtube_url',
        ),
        migrations.AddField(
            model_name='match',
            name='bramki',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
