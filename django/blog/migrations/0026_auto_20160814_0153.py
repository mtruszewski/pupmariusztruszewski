# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0025_auto_20160814_0151'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ad',
            name='button',
            field=models.ImageField(upload_to='buttons/', null=True, blank=True),
        ),
    ]
