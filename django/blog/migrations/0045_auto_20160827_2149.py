# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0044_auto_20160827_2132'),
    ]

    operations = [
        migrations.AddField(
            model_name='zawodnik',
            name='side',
            field=models.CharField(max_length=1, choices=[('GK', 'GK'), ('L', 'Left'), ('C', 'Center'), ('R', 'Right'), ('L/R', 'Left/Right'), ('L/C', 'Left/Center'), ('C/R', 'Center/Right'), ('L/C/R', 'Left/Center/Right')], default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='zawodnik',
            name='position',
            field=models.CharField(max_length=10, choices=[('GK', 'Goalkeeper'), ('DF', 'Defender'), ('MF', 'Midfielder'), ('FW', 'Forward')]),
        ),
    ]
