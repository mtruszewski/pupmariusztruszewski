# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0040_auto_20160827_1110'),
    ]

    operations = [
        migrations.AddField(
            model_name='team',
            name='points_away',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='team',
            name='points_home',
            field=models.IntegerField(default=0),
        ),
    ]
