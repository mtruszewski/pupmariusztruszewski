# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0061_delete_book'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tactics',
            name='tactic',
            field=models.CharField(max_length=1000),
        ),
    ]
