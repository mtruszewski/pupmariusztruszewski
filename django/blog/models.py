from django.db import models
from django.utils import timezone
from django.db.models import F, Case, When, Value, CharField
from django.utils.translation import ugettext as _
from django.db.models import Q, Sum
from django.shortcuts import render, get_object_or_404
from .functions import kto_wygral, podlicz_punkty, statystyki_zawodnika
from .choices import nationality_choices, position_choices

# Create your models here.

class Post(models.Model):
    author = models.ForeignKey('auth.User')
    title = models.CharField(max_length=100)
    text = models.TextField()
    obrazek = models.ImageField(upload_to='post/', blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.title


class Team(models.Model):
    author = models.ForeignKey('auth.User')
    team = models.CharField(max_length=30)
    win_home = models.IntegerField(default=0)
    draw_home = models.IntegerField(default=0)
    lose_home = models.IntegerField(default=0)
    strzelone_home = models.IntegerField(default=0)
    stracone_home = models.IntegerField(default=0)
    points_home = models.IntegerField(default=0)

    win_away = models.IntegerField(default=0)
    draw_away = models.IntegerField(default=0)
    lose_away = models.IntegerField(default=0)
    strzelone_away = models.IntegerField(default=0)
    stracone_away = models.IntegerField(default=0)
    points_away = models.IntegerField(default=0)

    win = models.IntegerField(default=0)
    draw = models.IntegerField(default=0)
    lose = models.IntegerField(default=0)
    points = models.IntegerField(default=0)



    def save(self, *args, **kwargs):
        podlicz_punkty(self, *args, **kwargs)
        super(Team, self).save(*args, **kwargs)

    def __str__(self):
        return self.team


class Zawodnik(models.Model):
    team = models.ForeignKey(Team)
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=50)
    number = models.IntegerField(default=0)
    app = models.IntegerField(default=0)
    timePlayed = models.IntegerField(default=0)
    goals = models.IntegerField(default=0)
    dateOfBirth = models.DateField(default=timezone.now)
    photo = models.ImageField(upload_to="photo/", blank=True, null=True)
    nationality = models.CharField(max_length=2, choices=nationality_choices)
    position = models.CharField(max_length=2, choices=position_choices)

    def __str__(self):
        return self.name + " " + self.surname


class Match(models.Model):
    author = models.ForeignKey('auth.User')
    team1 = models.ForeignKey(Team, null=True, related_name='team1')
    goal1 = models.IntegerField(blank=True, null=True)
    team2 = models.ForeignKey(Team, null=True, related_name='team2')
    goal2 = models.IntegerField(blank=True, null=True)
    wynik = models.CharField(default=0, max_length=400, help_text="Save to see")
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)


    def __str__(self):
        if self.goal1 is None:
            self.goal1 = ' '
        if self.goal2 is None:
            self.goal2 = ' '
        return str(self.team1) + " " + str(self.goal1) + " " + "vs" + " " + str(self.goal2)+" "+str(self.team2)


    def save(self, *args, **kwargs):
        if self.goal1 is None and self.goal2 is not None:
            self.goal1 = int(0)
            kto_wygral(self, *args, **kwargs)
        elif self.goal1 is not None and self.goal2 is None:
            self.goal2 = int(0)
            kto_wygral(self, *args, **kwargs)
        elif self.goal2 is not None and self.goal1 is not None:
            kto_wygral(self, *args, **kwargs)
        self.wynik = str(self.team1) + " " + str(self.goal1) + " " + "vs" + " " + str(self.goal2)+" "+str(self.team2)
        super(Match, self).save(*args, **kwargs)

class Sklad(models.Model):
    match = models.ForeignKey(Match)
    team1Players = models.ForeignKey(Zawodnik, blank=True, null=True, related_name='team1Players')
    goalPlayer1 = models.IntegerField(default=0)
    timePlayedP1 = models.IntegerField(default=0)
    team2Players = models.ForeignKey(Zawodnik, blank=True, null=True, related_name='team2Players')
    goalPlayer2 = models.IntegerField(default=0)
    timePlayedP2 = models.IntegerField(default=0)

    def __str__(self):
        if self.team1Players is not None and self.team2Players is not None:
            return "Dodano do skladu druzyny gospodarzy ("  + self.match.team1.team + ") " + " zawodnika: "+ self.team1Players.name + " " + self.team1Players.surname + " oraz do druzyny gosci: (" + self.match.team2.team + ")" + " zawodnika: " + self.team2Players.name + " " + self.team2Players.surname
        elif self.team1Players is not None and self.team2Players is None:
            return "Dodano do skladu druzyny gospodarzy ("  + self.match.team1.team + ") " + " zawodnika: "+ self.team1Players.name + " " + self.team1Players.surname
        elif self.team1Players is None and self.team2Players is not None:
            return "Dodano do skladu druzyny gosci: (" + self.match.team2.team + ")" + " zawodnika: " + self.team2Players.name + " " + self.team2Players.surname

    def save(self, *args, **kwargs):
        statystyki_zawodnika(self, *args, **kwargs)
        super(Sklad, self).save(*args, **kwargs)

class Tactics(models.Model):
    author = models.ForeignKey('auth.User')
    tactic = models.CharField(max_length=10000)
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return str(self.created_date)


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('Data utworzenia')

    def __str__(self):
        return self.question_text


class Choice(models.Model):
    poll = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField()

    def __str__(self):
        return self.choice_text


class Ad(models.Model):
    author = models.ForeignKey('auth.User')
    name_ad = models.CharField(max_length=50)
    link = models.URLField(max_length=200)
    button = models.ImageField(upload_to="buttons/", blank=True, null=True)
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.name_ad


class Video(models.Model):
    user = models.ForeignKey('auth.User')
    video_id = models.CharField(max_length=255, unique=True, null=True,
                                help_text=_("Youtube ID"))
    title = models.CharField(max_length=200, null=True, blank=True)
    published_date = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.title
