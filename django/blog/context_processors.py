from .models import Question, Team, Zawodnik, Ad, Video
from django.utils import timezone

def ankieta_processor(request):
    pytania_ankieta = Question.objects.filter(pub_date__lte=timezone.now()).order_by('-pub_date')[:5]
    ad = Ad.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
    youtube = Video.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
    return {'pytania_ankieta': pytania_ankieta, 'ad': ad, 'youtube':youtube}

def lista_druzyn(request):
    lista_druzyn = Team.objects.all().order_by('team')
    lista_zawodnikow = Zawodnik.objects.all().order_by('team')
    return {'lista_druzyn' : lista_druzyn, 'lista_zawodnikow': lista_zawodnikow}
