from django.shortcuts import render, get_object_or_404, render_to_response
from django.utils import timezone
from django.http import  HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.db.models import Q, Sum, F
from .models import Post
from .models import Team
from .models import Question
from .models import Choice
from .models import Match
from .models import Ad
from .models import Zawodnik
from .models import Sklad
from .models import Tactics
from .forms import TacticForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.models import User


def tactic(request, pk):
    team = get_object_or_404(Team, pk=pk)
    tacticAll = Tactics.objects.all()
    if request.method == 'POST':
        form = TacticForm(request.POST)
        if form.is_valid():
            user = User.objects.get(username=request.user)
            tactic = request.POST.get('tactic', '')
            tactic_obj = Tactics(tactic = tactic, author=user)
            tactic_obj.save()
    else:
        form = TacticForm()
    return render(request, 'blog/tactic.html', {'form': form, 'team': team, 'tacticAll': tacticAll})

def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
    paginator = Paginator(posts, 6)
    page = request.GET.get('page')
    try:
        posty = paginator.page(page)
    except PageNotAnInteger:
        posty = paginator.page(1)
    except EmptyPage:
        posty = paginator.page(paginator.num_pages)

    return render(request, 'blog/post_list.html', {'posts': posts, 'posty': posty})


def team(request):
    teams = Team.objects.all()\
        .annotate(goal_difference = F('strzelone_home')+F('strzelone_away')-F('stracone_home')-F('stracone_away'))\
        .annotate(goal_plus = F('strzelone_home')+F('strzelone_away'))\
        .order_by('-points', '-goal_difference', '-goal_plus')
    wyniki = Match.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
    kolejka1 = Match.objects.filter(published_date__range=["2016-07-15", "2016-07-19"]).order_by('-published_date')[:8]
    kolejka2 = Match.objects.filter(published_date__range=["2016-07-19", "2016-07-26"]).order_by('-published_date')[:8]
    kolejka3 = Match.objects.filter(published_date__range=["2016-07-27", "2016-08-02"]).order_by('-published_date')[:8]
    return render(request, 'blog/team.html', {'teams': teams, 'wyniki': wyniki, 'team_detail': team_detail,
                                              'kolejka1': kolejka1, 'kolejka2': kolejka2, 'kolejka3': kolejka3})


def team_fixture(request, pk):
    team_fixture = get_object_or_404(Team, pk=pk)
    wyniki = Match.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
    wyniki2 = Match.objects.filter(Q(team1=team_fixture) | Q(team2=team_fixture)).order_by('-published_date')
    wyniki2_away = Match.objects.filter(team2=team_fixture).order_by('-published_date')
    wyniki2_home = Match.objects.filter(team1=team_fixture).order_by('-published_date')
    bramki_home = Match.objects.filter(team1=team_fixture).aggregate(Sum('goal1')).get('goal1__sum', 0.00)
    bramki_away = Match.objects.filter(team2=team_fixture).aggregate(Sum('goal2')).get('goal2__sum', 0.00)
    stracone_home = Match.objects.filter(team1=team_fixture).aggregate(Sum('goal2')).get('goal2__sum', 0.00)
    stracone_away = Match.objects.filter(team2=team_fixture).aggregate(Sum('goal1')).get('goal1__sum', 0.00)
    return render(request, 'blog/terminarz_druzyny.html', {'team_fixture': team_fixture, 'wyniki': wyniki, 'wyniki2': wyniki2,
                                                           'wyniki2_home': wyniki2_home, 'wyniki2_away': wyniki2_away,
                                                           'bramki_home': bramki_home, 'bramki_away': bramki_away,
                                                           'stracone_home': stracone_home, 'stracone_away': stracone_away})

def match(request, pk):
    match = get_object_or_404(Match, pk=pk)
    return render(request, 'blog/mecz.html', {'match': match})


def team_detail(request, pk):
    team_detail = get_object_or_404(Team, pk=pk)
    return render(request, 'blog/zawodnicy.html', {'team_detail': team_detail})


def index(request):
    return render(request, 'blog/index.html', {'index': index})


def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})


def ankieta(request):
    latest_question_list = Question.objects.filter(pub_date__lte=timezone.now()).order_by('-pub_date')
    context = {'latest_question_list': latest_question_list}
    return render(request, 'blog/ankieta.html', context)


def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'blog/detail.html', {'question': question})


def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'blog/results.html', {'question': question})


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'blog/detail.html', {
            'question': question,
            'error_message': "Nie wybrałeś odpowiedzi",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        return HttpResponseRedirect(reverse('results', args=(question.id,)))
