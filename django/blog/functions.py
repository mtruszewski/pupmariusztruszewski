def kto_wygral(self, *args, **kwargs):
        if self.goal1 > self.goal2:
            self.team1.win_home += 1
            self.team2.lose_away += 1
            podlicz_bramki(self, *args, **kwargs)
        elif self.goal1 == self.goal2:
            self.team1.draw_home += 1
            self.team2.draw_away += 1
            podlicz_bramki(self, *args, **kwargs)
        elif self.goal1 < self.goal2:
            self.team2.win_away += 1
            self.team1.lose_home += 1
            podlicz_bramki(self, *args, **kwargs)

def podlicz_bramki(self, *args, **kwargs):
        self.team1.strzelone_home += self.goal1
        self.team1.stracone_home += self.goal2
        self.team2.strzelone_away += self.goal2
        self.team2.stracone_away += self.goal1
        self.team1.save()
        self.team2.save()

def podlicz_punkty(self, *args, **kwargs):
        self.win = self.win_home + self.win_away
        self.draw = self.draw_home + self.draw_away
        self.lose = self.lose_home + self.lose_away
        self.points = ((self.win)*3)+((self.draw)*1)
        self.points_home = ((self.win_home)*3)+((self.draw_home)*1)
        self.points_away = ((self.win_away) * 3) + ((self.draw_away) * 1)

def statystyki_zawodnika(self, *args, **kwargs):
        if self.team1Players is not None:
            self.team1Players.app += 1
        if self.team2Players is not None:
            self.team2Players.app += 1
        if self.timePlayedP1 != 0:
            self.team1Players.timePlayed += self.timePlayedP1
        if self.timePlayedP2 != 0:
            self.team2Players.timePlayed += self.timePlayedP2
        if self.goalPlayer1 != 0:
            self.team1Players.goals += self.goalPlayer1
        if self.goalPlayer2 != 0:
            self.team2Players.goals += self.goalPlayer2
        if self.team1Players is not None:
            self.team1Players.save()
        if self.team2Players is not None:
            self.team2Players.save()
