from django.conf.urls import url
from . import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^$', views.post_list, name='post_list'),
    url(r'^team/$', views.team, name='team'),
    url(r'^index/$', views.index, name='index'),
    url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post_detail'),
    url(r'^poll/$', views.ankieta, name='ankieta'),
    url(r'^poll/(?P<question_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^poll/(?P<question_id>[0-9]+)/results/$', views.results, name='results'),
    url(r'^poll/(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
    url(r'^team/(?P<pk>[0-9]+)/$', views.team_detail, name='team_detail'),
    url(r'^terminarz/team/(?P<pk>[0-9]+)/$', views.team_fixture, name='team_fixture'),
    url(r'^match/squad/(?P<pk>[0-9]+)/$', views.match, name='match'),
    url(r'^tactic/team/(?P<pk>[0-9]+)/$', views.tactic, name='tactic'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
