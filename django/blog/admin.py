from django.contrib import admin
from .models import Post, Team, Zawodnik, Match, Question, Choice, Ad, Sklad, Tactics
from .models import Video
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render, get_object_or_404, render_to_response
from django.db.models import Q


# Register your models here.
class SkladInLine(admin.TabularInline):
    model = Sklad
    fields = ('team1Players', 'timePlayedP1', 'goalPlayer1', 'team2Players', 'timePlayedP2', 'goalPlayer2')
    extra = 0

class ZawodnikInLine(admin.TabularInline):
    model = Zawodnik
    extra = 0

admin.site.register(Tactics)

class Post_view(admin.ModelAdmin):
    list_display = ('title', 'text', 'obrazek', 'published_date')
    list_editable = ('title', 'text', 'obrazek', 'published_date')
    list_display_links = None
    search_fields = ('title', 'text')
admin.site.register(Post, Post_view)


class Team_view(admin.ModelAdmin):
    list_display = ('team', 'win', 'draw', 'lose', 'points', 'win_home', 'draw_home', 'lose_home', 'points_home', 'win_away', 'draw_away', 'lose_away', 'points_away')
    list_editable = ('team', 'win', 'draw', 'lose', 'points', 'win_home', 'draw_home', 'lose_home', 'points_home', 'win_away', 'draw_away', 'lose_away', 'points_away')
    list_filter = ['team']
    search_fields = ['team']
    inlines = [ZawodnikInLine]
admin.site.register(Team, Team_view)


admin.site.register(Ad)


class Zawodnik_view(admin.ModelAdmin):
    list_display = ('name', 'surname', 'team', 'dateOfBirth', 'position', 'goals')
    list_editable = ('team', 'dateOfBirth', 'position')
    list_filter = ['team']
    list_display_links = ['name', 'surname']
    search_fields = ('name', 'surname', 'team', 'dateOfBirth')
admin.site.register(Zawodnik, Zawodnik_view)


class MatchFilter(admin.SimpleListFilter):
    title = 'druzyny'
    parameter_name = 'team'
    default_value = None

    def lookups(self, request, model_admin):
        lista_filtrow = ([druzyna.team for druzyna in Team.objects.all().order_by('team')])
        return [(t, t) for t in lista_filtrow]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(Q(team1__team=self.value()) | Q(team2__team=self.value()))
        else:
            return queryset


class Mecze(admin.ModelAdmin):
    list_display = ('wynik', 'team1', 'goal1', 'goal2', 'team2', 'published_date')
    list_editable = ('team1', 'goal1', 'goal2', 'team2', 'published_date')
    list_filter = (MatchFilter,)
    search_fields = ('wynik', 'team1', 'team2')
    inlines = [SkladInLine]
admin.site.register(Match, Mecze)

class Sklad_view(admin.ModelAdmin):
    list_display = ('match', 'team1Players', 'goalPlayer1', 'team2Players', 'goalPlayer2')
admin.site.register(Sklad, Sklad_view)

class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3

class QuestionAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'pub_date')
    inlines = [ChoiceInline]
admin.site.register(Question, QuestionAdmin)



class VideoAdmin(admin.ModelAdmin):
    list_display = ('title', 'video_id', 'published_date')
    list_editable = ('title', 'video_id', 'published_date')
    list_display_links = None
    search_fields = ('title', 'video_id')
admin.site.register(Video, VideoAdmin)
