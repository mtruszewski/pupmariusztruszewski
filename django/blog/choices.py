
nationality_choices =(
        ('PL', 'Poland'),
        ('BG', 'Bulgaria'),
        ('HR', 'Croatia'),
        ('CZ', 'Czech Republic'),
        ('DE', 'Germany'),
        ('HU', 'Hungary'),
        ('PT', 'Portugal'),
        ('RS', 'Serbia'),
        ('SI', 'Slovenia'),
        ('SK', 'Slovakia'),
    )

position_choices = (
        ('GK', 'Goalkeeper'),
        ('DF', 'Defender'),
        ('MF', 'Midfielder'),
        ('FW', 'Forward'),
    )
