from django import forms
from .models import Tactics

class TacticForm(forms.ModelForm):
   class Meta:
       model = Tactics
       exclude = ["user"]
       fields = ('tactic',)
