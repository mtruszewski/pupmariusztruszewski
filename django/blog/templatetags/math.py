from django import template
import datetime

register = template.Library()


@register.filter
def suma(value,arg):
    return int(0 if value is None else value) + int(0 if arg is None else arg)

@register.filter
def roznica(value,arg):
    return int(0 if value is None else value) - int(0 if arg is None else arg)

@register.filter
def dzielenie(value,arg):
    if (value == 0) or (arg == 0):
        return 0
    else:
        return value / arg

@register.filter
def mnozenie(value,arg):
    return value * arg

@register.filter
def wiek(value, arg):
    if (int(value) < int(arg)):
        return int(arg) - int(value)
    else:
        return int(value) - int(arg)
