// Naszym kolejnym zadaniem będzie napisanie gry przeglądarkowej do zgadywania haseł. System losuje 1 słowo z zadeklarowanych, a następnie wyświetla słowo zakodowane w sposób: _ _ _ _
// Po prawej stronie znajdywać będą się litery, wygenerowane skryptowo (tj. umieszone automatycznie), po wciśnięciu których nastąpi odsłona litery w słowie, jeśli taka istnieje. Dodatkowo, jeżeli użytkownik wciśnie na klawiaturze wybrany klawisz, litera też powinna się odsłonić. Jeżeli litera została wciśnięta, przycisk powinien zostać zablokowany tak, aby nie można było go ponownie użyć dla wybranego słowa.
// Powyżej listy liter do wciśnięcia znajdywać się powinna lista użytych już liter.
// Liczba prób zgadywania: 2 * długość_słowa,
// Dodatkowo użytkownik ma możliwość 3 razy zgadnięcia hasła w inpucie, który znajduje się nad listą zgadywanych wyrazó.
// UWAGA! Przyjmij, że w strukturze html znajduje się tylko i wyłącznie (w sekcji body) <div class="fortune"></div>, reszta elementów tworzona jest przez skrypt.



$(document).ready(function () {
    $('<div/>', {class: 'leftSide'}).prependTo('.fortune');
    $('<div/>', {class: 'rightSide'}).appendTo('.fortune');
    $('<div/>', {class: 'clear'}).appendTo('.fortune');
    $('<div/>', {class: 'guessBox'}).prependTo('.rightSide');
    $('<div/>', {class: 'counter'}).html('Ilość liter: <span></span>').appendTo('.rightSide');
    $('<div/>', {class: 'usedLettersBox'}).html('Użyte litery: <span class="usedLetters"></span>').appendTo('.rightSide');
    $('<div/>', {class: 'letterBox'}).appendTo('.rightSide');
    $('<input/>', {name: 'guess', id: 'guess', type: 'text', 'value' : 'Wpisz hasło' }).appendTo('.guessBox');
    $('<button/>', {class: 'guessBtn', id: 'guessBtn'}).html('Zgaduj').appendTo('.guessBox');
    $('<h1/>', {class: 'title'}).html('Koło fortuny').prependTo('.leftSide');
    $('<div/>', {class: 'phrase'}).html('________').appendTo('.leftSide');
    $('<div/>', {class: 'limit'}).html('Pozostało/y <span></span> proby').appendTo('.leftSide');

    /* generujemy buttony */
    for (var i = 65; i <= 90; i++) {
        var letter = String.fromCharCode(i)
        $('<button/>', {id: letter, class: 'guessLetter'}).html(letter).appendTo('.letterBox');
    }

    /* deklaracja słów */
    var words = ['oksymoron', 'jablko', 'banan', 'wrona', 'fajrant', 'urlop', 'przerwa', 'wolne', 'javascript'],
            currentWord = words[(Math.random() * words.length) | 0].toUpperCase();
    dashedWord = "_".repeat(currentWord.length);

    /* umieszczamy zakodowane slowo */
    $('.phrase').html(dashedWord);

    /* ustawiamy ilość prób */
    $('.counter').find('span').html(currentWord.length * 2);
    /* ilość prob dla "Zgaduj" */
    $('.limit').find('span').html(Math.round(currentWord.length / 2));

    function guessLetterGame(e) {

        var dashedWordTab = dashedWord.split(''),
                currentLetter = (e.type == 'keyup') ? String.fromCharCode(e.keyCode) : $(this).attr('id'),
                /* ustawiamy ilość prób 2 */
                $counter = $('.counter').find('span'),
                currentCounter = parseInt($counter.html()),
                $usedLetters = $('.usedLetters'),
                $letterExists = false;
        if (e.target.tagName == 'INPUT') {
            return;
        }
            for (var i = 0; i < dashedWord.length; i++) {
                if (currentWord.charAt(i) == currentLetter) {
                    dashedWordTab[i] = currentLetter;
                    $letterExists = true
                }
            }
            dashedWord = dashedWordTab.join('');
            $('.phrase').html(dashedWord);
            /* ustawiamy ilość prób 2 */
            $counter.html(currentCounter - 1);
            if ($usedLetters.html().length == 0) {
                $usedLetters.html(currentLetter);
            } else {
                $usedLetters.append(',  ' + currentLetter);
            }
                $('#' + currentLetter).attr('disabled', 'disabled');
            if ($letterExists) {
                $('#' + currentLetter).addClass('correct');
            } else {
                $('#' + currentLetter).addClass('incorrect');
                //$(this).attr("disabled", true).addClass('incorrect'); -> zmiana, w keyup (this) zmieni sie
            }
            /* sprawdzanie wygranej */
            if (checkForWin(dashedWord)) {
                return;
            }
            /* sprawdzanie przegranej */
            if (currentCounter == 1) {
                $('.guessLetter').attr("disabled", true).addClass('unclickable');
                $(document).off('keyup');
                $('.limit').html('Wykorzystałeś ilość prób! Przegrana. Hasło to: (' + currentWord + ')').addClass('lose');
                return;
            }

        }

    /* sprawdzanie wygranej - do funkcji*/
    function checkForWin(word) {
        if (word == currentWord) {
            $('.guessLetter').attr('disabled', 'disabled').addClass('unclickable'); //$('.guessLetter').off('click) -> alternatywne wylaczenie buttona
            $(document).off('keyup');
            $('.limit').html('Wygrana. Hasło to: (' + currentWord + ')').addClass('win');
            return true;
        }
        return false;
    }


    /* nasluchujemy klikniecia i podmieniamy literki */
    $('.guessLetter').on('click', guessLetterGame);

    $(document).on('keyup', guessLetterGame);

    $('#guessBtn').on('click', function () {
        checkForWin($('#guess').val().toUpperCase());
        /* ilość prob dla "Zgaduj" */
        $limit = $('.limit').find('span'),
        limit_attempt = parseInt($limit.html());
        $limit.html(limit_attempt -1);
        if (limit_attempt == 1) {
          $('#guessBtn').attr('disabled', 'disabled').addClass('unclickable');
          $('.guessLetter').attr('disabled', 'disabled').addClass('unclickable');
          $(document).off('keyup');
          $('.limit').html('Wykorzystałeś ilość prób! Przegrana. Hasło to: (' + currentWord + ')').addClass('lose');
          return;
        };
    });
});
