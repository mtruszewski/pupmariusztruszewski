//Napiszmy symulator banku. Stwórzmy obiekt, który będzie realizował wpłaty, wypłaty do banku, pozwoli się na zalogowanie za pomocą nr PESEL. Do przetrzymywania informacji nt wpłat i wypłat użyj odpowiedniej struktury danych (stos / kolejka). Symulator banku powinien zawierać pola pozwalające na logowanie, następnie po zalogowaniu na wpłatę, wypłatę, przejrzenie logów wpłat i wypłat.
//
//Bank powinien pozwolić na wcześniejsze zarejestrowanie klienta (można stworzyć kilku predefiniowanych do testów). Klienci powinni być przetrzymywani we wcześniej utworzonym obiekcie (tj. strukturze danych Klient)
// Definiujemy obiekt stosu
$(document).ready(function () {

    var Stack = function () {
        var elems = [];

        function empty() {
            return (elems.length === 0) ? true : false;
        }

        function push(arg) {
            elems.push(arg);
        }

        function pop() {
            return (elems.length === 0) ? null : elems.pop();
        }

        function top() {
            return (elems.length === 0) ? null : elems.length - 1;
        }

        function size() {
            return elems.length;
        }

        return {
            empty: empty,
            push: push,
            pop: pop,
            top: top,
            size: size
        }
    }

// Definiujemy obiekt reprezentujący pojedyńczego klienta.

    var Klient = function (opt) {
        this.imie = opt.imie || '';
        this.nazwisko = opt.nazwisko || '';
        this.PESEL = opt.PESEL || '';
        this.stanKonta = opt.stanKonta || 0.0;
        this.historia = new Stack();

        this.wplac = function (kwota) {
            this.stanKonta += Math.abs(kwota);
            this.historia.push(Math.abs(kwota));
        }

        this.wyplac = function (kwota) {
            this.stanKonta -= Math.abs(kwota);
            this.historia.push(-1 * Math.abs(kwota));
        }

    }

// Definiujemy tablicę klientów

    var klienci = {};


// Dodajemy przykładowego klienta
    klienci[54122912875] = new Klient({
        imie: 'Pawel',
        nazwisko: 'Wal',
        PESEL: 54122912875
    });

// Praca z klientem

// Wyświetlenie danych klienta
    console.log(klienci[54122912875].imie);

// Praca ze stosem
    klienci[54122912875].historia.push(1000);
    klienci[54122912875].historia.push(150);
    klienci[54122912875].historia.push(-300);
    klienci[54122912875].historia.push(90);

// Klonujemy obiekt stosu, gdyż przy zdejmowaniu z niego stos się opróżni
    var tempHist = Object.create(klienci[54122912875].historia)
    var histSize = tempHist.size();
    for (var i = 0; i < histSize; i++) {
        var tmpOper = tempHist.pop();
        if (tmpOper < 0)
            console.log("Wypłacono " + tmpOper + "PLN");
        else
            console.log("Wpłacono " + tmpOper + "PLN");
    }


    $('#loguj').on('click', function () {
        checkPesel($('#pesel').val());
    });

    $('#rejestracja').on('click', function () {
        $(document).find('.register').show();
    });

    $('#rejestruj').on('click', function () {
        if (klienci[($('#peselReg')).val()]) {
            $(document).find('.register').hide();
            return alert('Klient juz istnieje');
        } else {
            klienci[($('#peselReg')).val()] = new Klient({
                imie: $('#imie').val(),
                nazwisko: $('#nazwisko').val(),
                PESEL: $('#peselReg').val(),
            });
            $(document).find('.register').hide();
            return alert('Konto zostało zalozone');
        }

    });

    $('#wplac').on('click', function () {
        var kwota = parseInt(prompt('Podaj kwote: '));
        klienci[$('#pesel').val()].wplac(kwota);
        console.log(klienci[($('#pesel')).val()].stanKonta);
        $(document).find('#stan_konta').html("Stan konta: " + klienci[($('#pesel')).val()].stanKonta + " zł")
        historia();
    })

    $('#wyplac').on('click', function () {
        var kwota = parseInt(prompt('Podaj kwote: '));
        klienci[$('#pesel').val()].wyplac(kwota);
        console.log(klienci[($('#pesel')).val()].stanKonta);
        $(document).find('#stan_konta').html("Stan konta: " + klienci[($('#pesel')).val()].stanKonta + " zł")
        historia();
    })

    $('#historia').on('click', function () {
        historia();
        alert("Automatyczna historia, przycisk useless");
    })

    function historia() {
        var tempHist = Object.create(klienci[($('#peselReg')).val()].historia)
        var histSize = tempHist.size();
        for (var i = 0; i < histSize; i++) {
            var tmpOper = tempHist.pop();
            if (tmpOper < 0) {
                $('<tr/>').html("Wypłacono: " + tmpOper + "PLN").appendTo('#historia_konta').addClass('red');
                $(document).find('#history_title').show();
            } else {
                $('<tr/>').html("Wpłacono: " + tmpOper + "PLN").appendTo('#historia_konta').addClass('green');
                $(document).find('#history_title').show();
            }
        }
    }

    function checkPesel(pesel) {
        if (klienci[($('#pesel')).val()]) {
            alert('Podano prawidlowy pesel');
            $(document).find('.footer').show().find('span').html('Witaj użytkowniku: ' + $('#imie').val() + " " + $('#nazwisko').val());
            $(document).find('.header').hide();
        } else {
            alert('Bledny pesel');
        }
    }
});