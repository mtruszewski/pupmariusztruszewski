# -*- coding: utf-8 -*-
import random
def play():
    count = 0
    iloscGier = int(input("Podaj ilość gier: "));
    graczWin = 0
    pcWin = 0
    remis = 0
    papier = 0
    kamien = 0
    nozyce = 0
    papierPC = 0
    kamienPC = 0
    nozycePC = 0

    while (0 < iloscGier):
        x = input("Twój wybór (papier/kamień/nożyce): ")
        choicesTab = ["papier", "kamień", "nożyce"]
        graczNumer = choicesTab.index(x)
        losujNumer = random.randrange(0,3)
        wyborPC = choicesTab[losujNumer]
        print ("Gracz: " + x + " vs " + "PC: " +wyborPC)

        if (losujNumer == 0):
            papierPC = papierPC+1
        elif (losujNumer == 1):
            kamienPC = kamienPC+1
        elif (losujNumer == 2):
            nozycePC = nozycePC+1

        if (graczNumer == 0):
            papier = papier+1
        elif (graczNumer == 1):
            kamien = kamien+1
        elif (graczNumer == 2):
            nozyce = nozyce+1

        if ((graczNumer-losujNumer)%3 == 0):
            iloscGier = iloscGier-1
            remis = remis+1
            print ("Remis " + "\n")
        elif ((graczNumer-losujNumer)%3 == 2):
            iloscGier = iloscGier-1
            graczWin = graczWin+1
            print ("Wygrales " + "\n")
        elif ((graczNumer-losujNumer)%3 == 1):
            iloscGier = iloscGier-1
            pcWin = pcWin+1
            print ("Przegrales " + "\n")

    if graczWin > pcWin:
        print ("\n" + "Zwyciezyles wynikiem: " + str(graczWin) + "-" + str(pcWin))
    elif graczWin < pcWin:
        print ("\n" + "Przegrales wynikiem: " + str(graczWin) + "-" + str(pcWin))
    else:
        print ("\n" + "Remis: " + str(remis+graczWin) + "-" + str(remis+pcWin))

    print ("\n" + "Podsumowanie: ")
    print ("Gracz wygral: " + str(graczWin) + " raz/y ")
    print ("Remis był: " + str(remis) + " raz/y ")
    print ("Komputer wygral: " + str(pcWin) + " raz/y ")
    print ("Wybory gracza: " + "papier = " + str(papier) + ", kamień = " + str(kamien) + ", nozyce = " + str(nozyce))
    print ("Wybory komputera: " + "papier = " + str(papierPC) + ", kamień = " + str(kamienPC) + ", nozyce = " + str(nozycePC))

play()
